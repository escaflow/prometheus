local k = import 'ksonnet/ksonnet.beta.3/k.libsonnet';
local service = k.core.v1.service;
local daemonset = k.apps.v1beta2.daemonset;
local deployment = k.apps.v1beta2.deployment;
local container = deployment.mixin.spec.template.spec.containersType;
local volume = deployment.mixin.spec.template.spec.volumesType;
local containerVolumeMount = container.volumeMountsType;
local oauthVolumeName = 'grafana-oauth-secrets';
local secret = k.core.v1.secret;
{
  grafana+:: {
    deployment+: {
        spec+: {
            template+: {
                spec+: {
                    containers: std.map(function(c)c {
                            volumeMounts+: [containerVolumeMount.new(oauthVolumeName, '/etc/grafana/secrets/oauth/'),],
                        }, super.containers,),
                    volumes+: [volume.fromSecret(oauthVolumeName, 'grafana-oauth-secrets')],
                },
            },
        },
    },
  },
}