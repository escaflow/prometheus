local t = (import 'kube-thanos/thanos.libsonnet');

local addMixin = (import 'kube-prometheus/lib/mixin.libsonnet');
local thanosMixin = addMixin({
  name: 'thanos',
  // namespace: 'monitoring', // optional
  // dashboardFolder: 'Infra', // optional
  mixin: (import 'thanos-mixin/mixin.libsonnet'),
});
local lokiMixin = addMixin({
  name: 'loki',
  mixin: (import 'loki-mixin/mixin.libsonnet'),
});
local promtailMixin = addMixin({
  name: 'promtail',
  mixin: (import 'promtail-mixin/mixin.libsonnet'),
});
local certManagerMixin = addMixin({
  name: 'cert-manager',
  mixin: (import 'cert-manager-mixin/mixin.libsonnet'),
});


local tm = (import 'thanos-mixin/dashboards/dashboards.libsonnet') +
           (import 'thanos-mixin/alerts/alerts.libsonnet') +
           (import 'thanos-mixin/rules/rules.libsonnet') +
           (import 'thanos-mixin/config.libsonnet');

local kp = (import 'kube-prometheus/main.libsonnet') +
           (import 'kube-prometheus/addons/all-namespaces.libsonnet') + 
           (import 'kube-prometheus/addons/anti-affinity.libsonnet') +
           (import 'kube-prometheus/addons/podsecuritypolicies.libsonnet') +
           (import 'kube-prometheus/addons/managed-cluster.libsonnet') +
           (import 'imports/grafana-add-secrets.jsonnet') +
  {
    values+:: {
      common+: {
        namespace: 'monitoring', // This is the target Namespace for our deployment
        versions+:: {
          // https://github.com/prometheus/alertmanager/releases
          alertmanager: "0.23.0",
          // https://github.com/prometheus/node_exporter/releases
          nodeExporter: "1.2.2",
          // https://github.com/kubernetes/kube-state-metrics/releases
          // kubeStateMetrics: "2.2.0",
          // https://github.com/brancz/kube-rbac-proxy/releases
          kubeRbacProxy: "0.11.0",
          // https://github.com/coreos/prometheus-operator/releases
          prometheusOperator: "0.50.0",
          // https://github.com/prometheus/prometheus/releases
          prometheus: "2.29.2",
          grafana: '8.1.2',
        },
      },
      prometheus+:: {
        namespaces: [], // here we define other namespaces the operater can access
        resources: {
          requests: { cpu: '1000m', memory: '2048Mi' },
          limits: { cpu: '2000m', memory: '4096Mi' },
        },
        thanos: {
          version: 'v0.22.0',
          image: 'quay.io/thanos/thanos:' + kp.values.prometheus.thanos.version,
          objectStorageConfig: {
            key: 'thanos.yaml',  // How the file inside the secret is called
            name: 'thanos-objectstorage',  // This is the name of your Kubernetes secret with the config
          },
        },
      },
      alertmanager+: {
       config: importstr 'imports/alertmanager-config.yaml',
      },
      grafana+:: {
        config: { // http://docs.grafana.org/installation/configuration/
          sections: {
            server: { root_url: "https://grafana.minikube.local" },
            metrics: { enabled: true },
            auth: { oauth_auto_login: true },
            "auth.generic_oauth":  {
              name: "Keycloak",
              enabled: true,
              client_id: "grafana",
              client_secret: "$__file{/etc/grafana/secrets/oauth/gf_oauth_client_secret}",
              scopes: "openid profile email",
              auth_url: "https://sso.minikube.local/auth/realms/etc/protocol/openid-connect/auth",
              token_url: "https://sso.minikube.local/auth/realms/etc/protocol/openid-connect/token",
              api_url: "https://sso.minikube.local/auth/realms/etc/protocol/openid-connect/userinfo",
              role_attribute_path: "contains(groups[*], 'grafana-administrators') && 'Admin' || contains(groups[*], 'grafana-admin') && 'Admin' || contains(groups[*], 'grafana-editor') && 'Editor' || 'Viewer'",
            },
            security: {
              disable_initial_admin_creation: true,
              cookie_secure: true,
              cookie_samesite: true,
              allow_embedding: true,
              strict_transport_security: true,
            },
            smtp: {
              enabled: true,
              skip_verify: true,
              host: "smtp.mail.svc:25",
              from_address: "grafana@minikube.local",
            },
          },
        },
        plugins: ['grafana-piechart-panel'],
        datasources: [{
          name: 'Thanos',
          type: 'prometheus',
          access: 'proxy',
          orgId: 1,
          url: 'http://thanos-query.' + kp.values.common.namespace + '.svc:9090',
          version: 1,
          editable: false,
        },
        {
          name: 'Loki',
          type: 'loki',
          access: 'proxy',
          orgId: 1,
          url: 'http://loki.' + kp.values.common.namespace + '.svc:3100',
          version: 1,
          editable: false,
        }
        ],
        folderDashboards+:: {
          Ingress: {
            'traefik.json': (import 'dashboards/ingress/traefik.json'),
          } + certManagerMixin.grafanaDashboards,
          Monitoring: {}
            + thanosMixin.grafanaDashboards,
          Logging: {
            'loki-chunks.json': (import 'dashboards/logging/loki-chunks.json'),
            'loki-deletion.json': (import 'dashboards/logging/loki-deletion.json'),
            'loki-logs.json': (import 'dashboards/logging/loki-logs.json'),
            // 'loki-operational.json': (import 'dashboards/logging/loki-operational.json'), // to big
            'loki-reads.json': (import 'dashboards/logging/loki-reads.json'),
            'loki-reads-resources.json': (import 'dashboards/logging/loki-reads-resources.json'),
            'loki-retention.json': (import 'dashboards/logging/loki-retention.json'),
            'loki-writes.json': (import 'dashboards/logging/loki-writes.json'),
            'loki-writes-resources.json': (import 'dashboards/logging/loki-writes-resources.json'),
          }
            // sadly these are to big, so we import them directly
            // + lokiMixin.grafanaDashboards
            + promtailMixin.grafanaDashboards,
        },
      }, // grafana
     }, // values
     prometheus+:: {
      prometheus+: {
        spec+: {  // https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#prometheusspec
          retention: '30d',
          externalUrl: 'https://prom-sbg5.minikube.local',
          walCompression: true,
          storage: {
            volumeClaimTemplate: {
              apiVersion: 'v1',
              kind: 'PersistentVolumeClaim',
              spec: {
                accessModes: ['ReadWriteOnce'],
                resources: { requests: { storage: '100Gi' } },
              },
            },
          },  // storage
        },  // spec
      },  // prometheus
    },  // prometheus
    alertmanager+:: {
      alertmanager+: {
        spec+: {
          externalUrl: 'https://alm-sbg5.minikube.local',
        },
      },
    }, // alertmanager
  };

local kt = {
  config+:: {
    local cfg = self,
    namespace: kp.values.common.namespace,
    version: kp.values.prometheus.thanos.version ,
    image: 'quay.io/thanos/thanos:' + cfg.version,
    replicaLabels: ['prometheus_replica', 'rule_replica'],
    objectStorageConfig: {
      name: 'thanos-objectstorage',
      key: 'thanos.yaml',
    },
    volumeClaimTemplate: {
      spec: {
        accessModes: ['ReadWriteOnce'],
        resources: {
          requests: {
            storage: '10Gi',
          },
        },
      },
    },
  },
};

local ts = t.store(kt.config {
  replicas: 1,
  serviceMonitor: true,
});

local tq = t.query(kt.config {
  replicas: 1,
  stores: [
    'dnssrv+_grpc._tcp.prometheus-k8s-thanos-sidecar.%s.svc.cluster.local' % [kt.config.namespace],
    'dnssrv+_grpc._tcp.thanos-store.%s.svc.cluster.local' % [kp.values.common.namespace]
  ],
  serviceMonitor: true,
});

local tc = t.compact(kt.config {
  replicas: 1,
  serviceMonitor: true,
  retentionResolutionRaw: '30d',
  retentionResolution5m: '60d',
  retentionResolution1h: '720d',
  deduplicationReplicaLabels: kt.config.replicaLabels,
});

{ 'setup/0namespace-namespace': kp.kubePrometheus.namespace } +
{
  ['setup/prometheus-operator-' + name]: kp.prometheusOperator[name]
  for name in std.filter((function(name) name != 'serviceMonitor' && name != 'prometheusRule'), std.objectFields(kp.prometheusOperator))
} +
// serviceMonitor and prometheusRule are separated so that they can be created after the CRDs are ready
{ 'prometheus-operator-serviceMonitor': kp.prometheusOperator.serviceMonitor } +
{ 'prometheus-operator-prometheusRule': kp.prometheusOperator.prometheusRule } +
{ 'kube-prometheus-prometheusRule': kp.kubePrometheus.prometheusRule } +
{ ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
{ ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
{ ['kubernetes-' + name]: kp.kubernetesControlPlane[name] for name in std.objectFields(kp.kubernetesControlPlane) }
{ ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
{ ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
{ ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
{ ['thanos-store-' + name]: ts[name] for name in std.objectFields(ts) } +
{ ['thanos-query-' + name]: tq[name] for name in std.objectFields(tq) } +
{ ['thanos-compact-' + name]: tc[name] for name in std.objectFields(tc) } +
{ ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) } +
{ 'mixin-thanos-rules': thanosMixin.prometheusRules } + // one object for each mixin
{ 'mixin-loki-rules': lokiMixin.prometheusRules } +
{ 'mixin-promtail-rules': promtailMixin.prometheusRules } +
{ 'mixin-cert-manager-rules': certManagerMixin.prometheusRules }

